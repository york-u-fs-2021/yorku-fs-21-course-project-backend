import dotenv from 'dotenv'
dotenv.config()

import cors from 'cors'
import express from 'express'

import routes from './src/routes.js'
import portfolioRoutes from './src/portfolioRoutes.js'
import employmentRoutes from './src/employmentRoutes.js'
import educationRoutes from './src/educationRoutes.js'
import awardRoutes from './src/awardRoutes.js'

const port = process.env.ENV_PORT || 8080
const app = express()

app.use(cors())

app.use(express.json())

app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*')
    next()
})

app.use('/', routes)
app.use('/', portfolioRoutes)
app.use('/', employmentRoutes)
app.use('/', educationRoutes)
app.use('/', awardRoutes)

// Global error handler
app.use((err, req, res, next) => {
    console.error(err.stack)
    return res.status(404).json("Error 404: not found.")
})

app.listen(port, () => console.log(`API server listening on port ${port}`))