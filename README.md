# YorkU CSFS 2021 Program Project - Backend

## Description

A backend application, using Node and Express, for my personal portfolio website.

## Local Dev Environment

To run the api locally you will need a .env file in the root folder. In this file add the following variables and assign appropriate values: 
* ENV_PORT = any available port number (ex: 4000)
* JWT_SECRET 
* DATABASE_HOST = localhost
* DATABASE_NAME
* DATABASE_PW 
* DATABASE_ROOT_PW
* DATABASE_USER

Grab the setup.sql file from the [database repo](https://gitlab.com/york-u-fs-2021/yorku-fs-21-course-project-database) and run it using a tool like MySQL Workbench or dbeaver.

### Dependencies

After cloning this project, in your Terminal (while in the backend folder) run the command "npm install" which will create a node_modules folder that contains the project's dependencies as outlined in the package.json file. Afterwards, in your Terminal enter "npm start" to run the application (or "npm run dev"; see scripts section below).

## Deployment

Continuous deployment has been set up for this backend project using GitLab CI/CD variables. When changes are pushed/merged to the master branch they will be automatically deployed provided the CI pipeline passes (see the .gitlab-ci.yml files for details).

*Note:* after the initial deployment in GCP I had to connect to the Cloud SQL instance and run the setup.sql file (as mentioned above).

## Scripts

These are shortcuts for commands you can enter in the Terminal. ex: you could enter "npm start" instead of "npm node index.js". Use "npm start" to get the backend running.

"start": "node -r esm index.js"

"dev": "nodemon -r esm index.js" (ex: "npm run dev")







