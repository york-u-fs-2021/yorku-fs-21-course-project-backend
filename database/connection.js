// for local dev environment:
import dotenv from 'dotenv';
dotenv.config();

const mysql = require("mysql");

const configuration = {
  user: process.env.DATABASE_USER,
  password: process.env.DATABASE_PW,
  database: process.env.DATABASE_NAME
};

if (process.env.DATABASE_SOCKET) {
  configuration.socketPath = process.env.DATABASE_SOCKET;
} else {
  configuration.host = process.env.DATABASE_HOST;
}

const connection = mysql.createConnection(configuration);

connection.connect(function (err){
  if (err) {
    console.log(err);
  } else {
    console.log("MySQL database is connected");
  }
});

export default connection;