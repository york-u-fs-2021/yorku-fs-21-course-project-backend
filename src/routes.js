//Dependency imports:
import dotenv from 'dotenv' ;
dotenv.config();
import express from 'express' ;
import { v4 as uuidv4 } from 'uuid'; // function uuidv4() to create a random uuid
import jwt from 'jsonwebtoken' ;

//Custom module imports:
import { validateCF, validateUser } from './middleware/validation.js' ;
import { createHash, verifyHash } from './util/hasher.js' ;
import jwtVerify from './middleware/jwtVerify.js' ;
import database from '../database/connection.js';

const router = express.Router() ;

//1. Route to create an entry when the user submits their contact form
router.post('/contact_form/entries', validateCF, async (req, res, next) => {
    const newEntry = {
        messageID: uuidv4(),
        ...req.body
    };
    const {messageID, name, email, phoneNumber, content} = newEntry;
    try {
        database.query(
            "INSERT INTO messages VALUES (?,?,?,?,?)",
            [messageID, name, email, phoneNumber, content],
            function (error, results, fields){
                if (error) throw error;
            }
        );
        return res.status(201).json(newEntry);
    } catch (err) {
        console.error(err);
        return next(err);
    };
});

//2. Route to create a user
router.post('/users', jwtVerify, validateUser, (req, res, next) => {
    //check if a user with the email given already exists in the database:
    database.query("SELECT email FROM users WHERE email = ?", [req.body.email],
        function (error, results, fields){
            if (results.length){
                return res.status(400).json('Error: an account with this email address already exists.');
            };
            //if email is unique then continue by hashing the password:
            let password = req.body.password;
            createHash(password).then(hash => {
                req.body.password = hash;
                return req.body;            
            }).then(async (newUser) => {
                const {name, password, email} = newUser;
                try { //lastly, add new user to database:
                    database.query(
                        "INSERT INTO users (name, password, email) VALUES (?,?,?)",
                        [name, password, email],
                        function (error, results, fields){
                            if (error) throw error;
                        }
                    );
                    delete newUser.password;
                    return res.status(201).json(newUser);
                } catch (err) {
                    console.error(err);
                    return next(err);
                };
            });
        }
    )    
});
  
//3. Route to log in a registered user to create a JWT
router.post('/auth', (req, res) => {
    database.query(
        "SELECT * FROM users WHERE email = ?",
        [req.body.email],
        function (error, results, fields) {
            if (error) throw error;
            const user = results[0];
            //In the event the email or password does not match: 
            if (!user) {
                return res.status(401).json("Incorrect credentials provided");
            };
            let password = req.body.password;
            let storedHash = user.password;
            verifyHash(password, storedHash).then(valid => {
                if (!valid) {
                    return res.status(401).json("Incorrect credentials provided");
                };
                // Upon successful login: 
                const userEmail = req.body.email;
                const token = jwt.sign({userEmail}, process.env.JWT_SECRET, {expiresIn: "2h"});      
                return res.json({token});
            });
        }
    ); 
});

//4. Route to get a listing of all submissions when given a valid JWT is provided from the route defined above
router.get('/contact_form/entries', jwtVerify, async (req, res, next) => {   
    try {
        database.query(
            "SELECT * FROM messages",
            function (error, results, fields) {
                if (error) throw error;
                return res.json(results);
            }
        );
    } catch (err) {
        console.error(err);
        return next(err);
    };
});

//5. Route to get a specific submission when given an ID alongside a valid JWT
router.get('/contact_form/entries/:id', jwtVerify, async (req, res, next) => {
    const {id} = req.params;
    try {
        database.query(
            "SELECT * FROM messages WHERE messageID = ?",
            [id],
            function (error, results, fields){
                if (error) throw error;
                const entry = results[0];
                if (!entry) {
                    return res.status(404).json({message: `entry ${id} not found`});
                };
                return res.json(entry);
            }
        );       
    } catch (err) {
        console.error(err);
        return next(err);
    };
});

export default router
