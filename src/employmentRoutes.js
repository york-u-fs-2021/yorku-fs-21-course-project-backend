//Dependency imports:
import dotenv from 'dotenv' ;
dotenv.config();
import express from 'express' ;

//Custom module imports:
import { emptyValidation } from './middleware/validation.js' ;
import jwtVerify from './middleware/jwtVerify.js' ;
import database from '../database/connection.js';

const router = express.Router() ;

//Employment routes

//>>>8.A) route to get all employment items
router.get('/resume/employment', async (req, res, next) => {    
    try {
        database.query(
            "SELECT * FROM employment_item",
            function (error, results) {
                if (error) throw error;
                return res.json(results);
            }
        );
    } catch (err) {
        console.error(err);
        return next(err);
    };
});

//>>>8.B) route to add a new employment item
router.post('/resume/employment', jwtVerify, emptyValidation, async (req, res, next) => {
    const {jobPosition, place, dateRange} = req.body;
    try {
        database.query(
            "INSERT INTO employment_item (jobPosition, place, dateRange) VALUES (?,?,?)",
            [jobPosition, place, dateRange],
            function (error, results, fields){
                if (error) throw error;
            }
        );
        return res.status(201).json("New portfolio item successfully added.");
    } catch (err) {
        console.error(err);
        return next(err);
    }
});

//>>>8.C) route to delete an employment item (all associated bullet points will be deleted too)
router.delete('/resume/employment/:id', jwtVerify, async (req, res, next) => {
    try {
        database.query(
            "DELETE FROM employment_item WHERE itemID=?",
            [req.params.id],
            function (error, results, fields){
                if (error) throw error;
                let removed = results.affectedRows;
                if (!removed){
                    return res.status(404).json({message: `item ${req.params.id} not found`});
                };
                return res.status(204).json()
            }
        );
    } catch (err) {
        console.error(err);
        return next(err);
    };
});

//>>>8.D) route to get a specific employment item
router.get('/resume/employment/:id', jwtVerify, async (req, res, next) => {
    const {id} = req.params;
    try {
        database.query(
            "SELECT * FROM employment_item WHERE itemID=?",
            [id],
            function (error, results, fields){
                if (error) throw error;
                const item = results[0];
                if (!item) {
                    return res.status(404).json({message: `item ${id} not found`});
                };
                return res.json(item);
            }
        );       
    } catch (err) {
        console.error(err);
        return next(err);
    };
});

//>>>8.E) route to update an employment item
router.put('/resume/employment/:id', jwtVerify, emptyValidation, async (req, res, next) => {
    const {id} = req.params;
    const {jobPosition, place, dateRange} = req.body;
    try {
        database.query(
            "UPDATE employment_item SET jobPosition=?, place=?, dateRange=? WHERE itemID=?",
            [jobPosition, place, dateRange, id],
            function (error, results, fields){
                if (error) throw error;
                let updated = results.affectedRows;
                if (!updated){
                    return res.status(400).json(`Error: item ${id} not found`);
                };
                return res.json(updated);
            }
        );
    } catch (err) {
        console.error(err);
        return next(err);
    };
});

//>>>9.A) route to get all bullet points for employment items
router.get('/resume/employment-points', async (req, res, next) => {    
    try {
        database.query(
            "SELECT * FROM emp_bullet_point",
            function (error, results) {
                if (error) throw error;
                return res.json(results);
            }
        );
    } catch (err) {
        console.error(err);
        return next(err);
    };
});

//>>>9.B) route to add one or more bullet points to an employment item
router.post('/resume/employment-points', jwtVerify, emptyValidation, async (req, res, next) => {
    const {itemID, points} = req.body;
    const formattedPoints = points.split('\n');
    let mysqlQuery = "INSERT INTO emp_bullet_point (itemID, point) VALUES";
    let values = [];
    formattedPoints.forEach(point => {
        values.push(itemID, point);
        mysqlQuery += " (?,?),";
    });
    mysqlQuery = mysqlQuery.slice(0, -1); //used to remove the last comma in the string
    try {
        database.query(mysqlQuery, values,
            function (error, results, fields){
                if (error) throw error;
            }
        );
        return res.status(201).json("Successfully added bullet point(s).");
    } catch (err) {
        console.error(err);
        return next(err);
    }
});

//>>>9.C) route to delete a specific bullet point
router.delete('/resume/employment-points/:id', jwtVerify, async (req, res, next) => {
    try {
        database.query(
            "DELETE FROM emp_bullet_point WHERE pointID=?",
            [req.params.id],
            function (error, results, fields){
                if (error) throw error;
                let removed = results.affectedRows;
                if (!removed){
                    return res.status(404).json({message: `item ${req.params.id} not found`});
                };
                return res.status(204).json()
            }
        );
    } catch (err) {
        console.error(err);
        return next(err);
    };
});

//>>>9.D) route to update a specific bullet point
router.put('/resume/employment-points/:id', jwtVerify, emptyValidation, async (req, res, next) => {
    const {id} = req.params;
    const {point} = req.body;
    try {
        database.query(
            "UPDATE emp_bullet_point SET point=? WHERE pointID=?",
            [point, id],
            function (error, results, fields){
                if (error) throw error;
                let updated = results.affectedRows;
                if (!updated){
                    return res.status(404).json({message: `item ${id} not found`});
                };
                return res.json(updated);
            }
        );
    } catch (err) {
        console.error(err);
        return next(err);
    };
});

export default router
