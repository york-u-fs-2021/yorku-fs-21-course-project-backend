//Dependency imports:
import dotenv from 'dotenv' ;
dotenv.config();
import express from 'express' ;

//Custom module imports:
import jwtVerify from './middleware/jwtVerify.js' ;
import database from '../database/connection.js';
import { emptyValidation } from './middleware/validation.js';

const router = express.Router() ;

//Award routes

//>>>12.A) route to get all award items
router.get('/resume/awards', async (req, res, next) => {    
    try {
        database.query(
            "SELECT * FROM award_item",
            function (error, results) {
                if (error) throw error;
                return res.json(results);
            }
        );
    } catch (err) {
        console.error(err);
        return next(err);
    };
});

//>>>12.B) route to add a new award item
router.post('/resume/awards', jwtVerify, emptyValidation, async (req, res, next) => {
    const {awardName, year, description} = req.body;
    try {
        database.query(
            "INSERT INTO award_item (awardName, year, description) VALUES (?,?,?)",
            [awardName, year, description],
            function (error, results, fields){
                if (error) throw error;
            }
        );
        return res.status(201).json("New award item successfully added.");
    } catch (err) {
        console.error(err);
        return next(err);
    }
});

//>>>12.C) route to delete an award item 
router.delete('/resume/awards/:id', jwtVerify, async (req, res, next) => {
    try {
        database.query(
            "DELETE FROM award_item WHERE itemID=?",
            [req.params.id],
            function (error, results, fields){
                if (error) throw error;
                let removed = results.affectedRows;
                if (!removed){
                    return res.status(404).json({message: `item ${req.params.id} not found`});
                };
                return res.status(204).json()
            }
        );
    } catch (err) {
        console.error(err);
        return next(err);
    };
});

//>>>12.D) route to get a specific award item
router.get('/resume/awards/:id', jwtVerify, async (req, res, next) => {
    const {id} = req.params;
    try {
        database.query(
            "SELECT * FROM award_item WHERE itemID=?",
            [id],
            function (error, results, fields){
                if (error) throw error;
                const item = results[0];
                if (!item) {
                    return res.status(404).json({message: `item ${id} not found`});
                };
                return res.json(item);
            }
        );       
    } catch (err) {
        console.error(err);
        return next(err);
    };
});

//>>>12.E) route to update an award item
router.put('/resume/awards/:id', jwtVerify, emptyValidation, async (req, res, next) => {
    const {id} = req.params;
    const {awardName, year, description} = req.body;
    try {
        database.query(
            "UPDATE award_item SET awardName=?, year=?, description=? WHERE itemID=?",
            [awardName, year, description, id],
            function (error, results, fields){
                if (error) throw error;
                let updated = results.affectedRows;
                if (!updated){
                    return res.status(400).json(`Error: item ${id} not found`);
                };
                return res.json(updated);
            }
        );
    } catch (err) {
        console.error(err);
        return next(err);
    };
});

export default router