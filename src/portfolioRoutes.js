//Dependency imports:
import dotenv from 'dotenv' ;
dotenv.config();
import express from 'express' ;

//Custom module imports:
import { validatePFitem } from './middleware/validation.js' ;
import jwtVerify from './middleware/jwtVerify.js' ;
import database from '../database/connection.js';

const router = express.Router() ;

//Portfolio routes

//>>>6.A) route to add a new portfolio section
router.post('/portfolio', jwtVerify, async (req, res, next) => {
    const {sectionTitle, sectionDescription, order} = req.body;
    try {
        database.query(
            "INSERT INTO portfolio_section (sectionTitle, sectionDescription, `order`) VALUES (?,?,?)",
            [sectionTitle, sectionDescription, order],
            function (error, results, fields){
                if (error) throw error;
            }
        );
        return res.status(201).json("New section successfully added.");
    } catch (err) {
        console.error(err);
        return next(err);
    }
});

//>>>6.B) route to get all portfolio sections
router.get('/portfolio', async (req, res, next) => {
    try {
        database.query(
            "SELECT * FROM portfolio_section ORDER BY `order`, sectionID",
            function (error, results, fields) {
                if (error) throw error;
                return res.json(results);
            }
        );
    } catch (err) {
        console.error(err);
        return next(err);
    };
})

//>>>6.C) route to delete a portfolio section
router.delete('/portfolio/:id', jwtVerify, async (req, res, next) => {
    try {
        database.query(
            "DELETE FROM portfolio_section WHERE sectionID=?",
            [req.params.id],
            function (error, results, fields){
                if (error) throw error;
                let removed = results.affectedRows;
                if (!removed){
                    return res.status(404).json({message: `section ${req.params.id} not found`});
                };
                return res.status(204).json() // it is an empty response as this resource no longer exists
            }
        );
    } catch (err) {
        console.error(err);
        return next(err);
    };
});

//>>>6.D) route to get a specific portfolio section
router.get('/portfolio/:id', jwtVerify, async (req, res, next) => {
    const {id} = req.params;
    try {
        database.query(
            "SELECT * FROM portfolio_section WHERE sectionID=?",
            [id],
            function (error, results, fields){
                if (error) throw error;
                const section = results[0];
                if (!section) {
                    return res.status(404).json({message: `section ${id} not found`});
                };
                return res.json(section);
            }
        );       
    } catch (err) {
        console.error(err);
        return next(err);
    };
});

//>>>6.E) route to update a portfolio section
router.put('/portfolio/:id', jwtVerify, async (req, res, next) => {
    const {id} = req.params;
    const {sectionTitle, sectionDescription, order} = req.body;
    try {
        database.query(
            "UPDATE portfolio_section SET sectionTitle=?, sectionDescription=?, `order`=? WHERE sectionID=?",
            [sectionTitle, sectionDescription, order, id],
            function (error, results, fields){
                if (error) throw error;
                let updated = results.affectedRows;
                if (!updated){
                    return res.status(400).json(`Error: section ${id} not found`);
                };
                return res.json(updated);
            }
        );
    } catch (err) {
        console.error(err);
        return next(err);
    };
});

//>>>7.A) route to add a new portfolio item
router.post('/portfolio/items', jwtVerify, validatePFitem, async (req, res, next) => {
    const {sectionID, itemTitle, itemDescription, codeLink, demoLink, screenshot, altText} = req.body;
    try {
        database.query(
            "INSERT INTO portfolio_item (sectionID, itemTitle, itemDescription, codeLink, demoLink, screenshot, altText) VALUES (?,?,?,?,?,?,?)",
            [sectionID, itemTitle, itemDescription, codeLink, demoLink, screenshot, altText],
            function (error, results, fields){
                if (error) throw error;
            }
        );
        return res.status(201).json("New portfolio item successfully added.");
    } catch (err) {
        console.error(err);
        return next(err);
    }
});

//>>>7.B) route to get all portfolio items
router.get('/portfolio-items', async (req, res, next) => {
    try {
        database.query(
            "SELECT * FROM portfolio_item",
            function (error, results, fields) {
                if (error) throw error;
                return res.json(results);
            }
        );
    } catch (err) {
        console.error(err);
        return next(err);
    };
})

//>>>7.C) route to delete a portfolio item
router.delete('/portfolio-items/:id', jwtVerify, async (req, res, next) => {
    try {
        database.query(
            "DELETE FROM portfolio_item WHERE itemID=?",
            [req.params.id],
            function (error, results, fields){
                if (error) throw error;
                let removed = results.affectedRows;
                if (!removed){
                    return res.status(404).json({message: `item ${req.params.id} not found`});
                };
                return res.status(204).json() // it is an empty response as this resource no longer exists
            }
        );
    } catch (err) {
        console.error(err);
        return next(err);
    };
});

//>>>7.D) route to get a specific portfolio item
router.get('/portfolio-items/:id', jwtVerify, async (req, res, next) => {
    const {id} = req.params;
    try {
        database.query(
            "SELECT * FROM portfolio_item WHERE itemID=?",
            [id],
            function (error, results, fields){
                if (error) throw error;
                const section = results[0];
                if (!section) {
                    return res.status(404).json({message: `item ${id} not found`});
                };
                return res.json(section);
            }
        );       
    } catch (err) {
        console.error(err);
        return next(err);
    };
});

//>>>7.E) route to update a portfolio item
router.put('/portfolio-items/:id', jwtVerify, validatePFitem, async (req, res, next) => {
    const {id} = req.params;
    const {sectionID, itemTitle, itemDescription, codeLink, demoLink, screenshot, altText} = req.body;
    try {
        database.query(
            "UPDATE portfolio_item SET sectionID=?, itemTitle=?, itemDescription=?, codeLink=?, demoLink=?, screenshot=?, altText=? WHERE itemID=?",
            [sectionID, itemTitle, itemDescription, codeLink, demoLink, screenshot, altText, id],
            function (error, results, fields){
                if (error) throw error;
                let updated = results.affectedRows;
                if (!updated){
                    return res.status(400).json(`Error: item ${id} not found`);
                };
                return res.json(updated);
            }
        );
    } catch (err) {
        console.error(err);
        return next(err);
    };
});

export default router