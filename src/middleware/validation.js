const isEmpty = field => field.toString().trim() === "" ? true : false;
//trim() removes whitespace from both ends of a string

const validateEmail = email => {
    const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
};

/*A function to check if the object sent in a request is missing any of the required properties and if the values are empty or incorrect.
It takes three parameters:
-fields: an array of required fields as specified in functions below
-obj: the object sent in the request
-msg: an array of error messages that can be sent in the response
*/
const checkProps = (fields, obj, msg) => {
    fields.forEach(field => {
        if (!obj.hasOwnProperty(field)) {
            msg.push(field);
        } else {
            switch (field) {
                case "email":
                    if (isEmpty(obj.email) || !validateEmail(obj.email)) {
                        msg.push(field);
                    };
                    break;
                case "phoneNumber":
                    if (!isEmpty(obj.phoneNumber) && !obj.phoneNumber.match(/^[0-9]{10}$/)) {
                        msg.push(field);
                    };
                    break;
                case "password":
                    if (isEmpty(obj.password) || obj.password.length < 8 || obj.password.includes(" ")) {
                        msg.push(field);
                    };
                    break;  
                default: //for any other required field where we just need to check if it's empty
                    if (isEmpty(obj[`${field}`])) {
                        msg.push(field);
                    };             
            };
        };
    });
};

const validateCF = (req, res, next) => {
    let reqFields = ["name", "email", "phoneNumber", "content"];  
    let errMsg = [];   
    checkProps(reqFields, req.body, errMsg); 
    if (errMsg.length) {
        return res.status(400).json(`Message: validation error. Invalid entries for ${errMsg.join(', ')}`); 
    };
    next();
};

const validateUser = (req, res, next) => {
    let reqFields = ["name", "password", "email"];
    let errMsg = [];
    checkProps(reqFields, req.body, errMsg);
    if (errMsg.length) { 
        return res.status(400).json(`Message: validation error. Invalid entries for ${errMsg.join(', ')}`); 
    };    
    next();
};

//use this function when all fields are required and it's only needed to check if form inputs are empty
const emptyValidation = (req, res, next) => {
    let errMsg = [];
    for (let key in req.body) {
        if (isEmpty(req.body[key])) {
            errMsg.push(key);
        };
    };
    if (errMsg.length) { 
        return res.status(400).json(`Message: validation error. Please make sure to fill in the required fields: ${errMsg.join(', ')}.`); 
    };    
    next();
};

const validateEduItem = (req, res, next) => {
    let reqFields = ["credentialName", "place", "dateRange"];
    let errMsg = [];
    checkProps(reqFields, req.body, errMsg);
    if (errMsg.length) { 
        return res.status(400).json(`Message: validation error. Invalid entries for ${errMsg.join(', ')}`); 
    };    
    next();
};

const validatePFitem = (req, res, next) => {
    let reqFields = ["sectionID", "itemTitle", "itemDescription"];
    let errMsg = [];
    checkProps(reqFields, req.body, errMsg);
    if (errMsg.length) { 
        return res.status(400).json(`Message: validation error. Invalid entries for ${errMsg.join(', ')}`); 
    };    
    next();
};

export { validateCF, validateUser, emptyValidation, validateEduItem, validatePFitem };