//Dependency imports:
import dotenv from 'dotenv' ;
dotenv.config();
import express from 'express' ;

//Custom module imports:
import jwtVerify from './middleware/jwtVerify.js' ;
import database from '../database/connection.js';
import { validateEduItem, emptyValidation } from './middleware/validation.js'

const router = express.Router() ;

//Education routes

//>>>10.A) route to get all education items
router.get('/resume/education', async (req, res, next) => {    
    try {
        database.query(
            "SELECT * FROM education_item",
            function (error, results) {
                if (error) throw error;
                return res.json(results);
            }
        );
    } catch (err) {
        console.error(err);
        return next(err);
    };
});

//>>>10.B) route to add a new education item
router.post('/resume/education', jwtVerify, validateEduItem, async (req, res, next) => {
    const {credentialName, place, dateRange, image, altText} = req.body;
    try {
        database.query(
            "INSERT INTO education_item (credentialName, place, dateRange, image, altText) VALUES (?,?,?,?,?)",
            [credentialName, place, dateRange, image, altText],
            function (error, results, fields){
                if (error) throw error;
            }
        );
        return res.status(201).json("New education item successfully added.");
    } catch (err) {
        console.error(err);
        return next(err);
    }
});

//>>>10.C) route to delete an education item (all associated bullet points will be deleted too)
router.delete('/resume/education/:id', jwtVerify, async (req, res, next) => {
    try {
        database.query(
            "DELETE FROM education_item WHERE itemID=?",
            [req.params.id],
            function (error, results, fields){
                if (error) throw error;
                let removed = results.affectedRows;
                if (!removed){
                    return res.status(404).json({message: `item ${req.params.id} not found`});
                };
                return res.status(204).json()
            }
        );
    } catch (err) {
        console.error(err);
        return next(err);
    };
});

//>>>10.D) route to get a specific education item
router.get('/resume/education/:id', jwtVerify, async (req, res, next) => {
    const {id} = req.params;
    try {
        database.query(
            "SELECT * FROM education_item WHERE itemID=?",
            [id],
            function (error, results, fields){
                if (error) throw error;
                const item = results[0];
                if (!item) {
                    return res.status(404).json({message: `item ${id} not found`});
                };
                return res.json(item);
            }
        );       
    } catch (err) {
        console.error(err);
        return next(err);
    };
});

//>>>10.E) route to update an education item
router.put('/resume/education/:id', jwtVerify, validateEduItem, async (req, res, next) => {
    const {id} = req.params;
    const {credentialName, place, dateRange, image, altText} = req.body;
    try {
        database.query(
            "UPDATE education_item SET credentialName=?, place=?, dateRange=?, image=?, altText=? WHERE itemID=?",
            [credentialName, place, dateRange, image, altText, id],
            function (error, results, fields){
                if (error) throw error;
                let updated = results.affectedRows;
                if (!updated){
                    return res.status(404).json({message: `item ${id} not found`});
                };
                return res.json(updated);
            }
        );
    } catch (err) {
        console.error(err);
        return next(err);
    };
});

//>>>11.A) route to get all bullet points for employment items
router.get('/resume/education-points', async (req, res, next) => {    
    try {
        database.query(
            "SELECT * FROM edu_bullet_point",
            function (error, results) {
                if (error) throw error;
                return res.json(results);
            }
        );
    } catch (err) {
        console.error(err);
        return next(err);
    };
});

//>>>11.B) route to add one or more bullet points to an education item
router.post('/resume/education-points', jwtVerify, emptyValidation, async (req, res, next) => {
    const {itemID, points} = req.body;
    const formattedPoints = points.split('\n');
    let mysqlQuery = "INSERT INTO edu_bullet_point (itemID, point) VALUES";
    let values = [];
    formattedPoints.forEach(point => {
        values.push(itemID, point);
        mysqlQuery += " (?,?),";
    });
    mysqlQuery = mysqlQuery.slice(0, -1); //used to remove the last comma in the string
    try {
        database.query(mysqlQuery, values,
            function (error, results, fields){
                if (error) throw error;
            }
        );
        return res.status(201).json("Successfully added bullet point(s).");
    } catch (err) {
        console.error(err);
        return next(err);
    }
});

//>>>11.C) route to delete a specific bullet point
router.delete('/resume/education-points/:id', jwtVerify, async (req, res, next) => {
    try {
        database.query(
            "DELETE FROM edu_bullet_point WHERE pointID=?",
            [req.params.id],
            function (error, results, fields){
                if (error) throw error;
                let removed = results.affectedRows;
                if (!removed){
                    return res.status(404).json({message: `item ${req.params.id} not found`});
                };
                return res.status(204).json()
            }
        );
    } catch (err) {
        console.error(err);
        return next(err);
    };
});

//>>>11.D) route to update a specific bullet point
router.put('/resume/education-points/:id', jwtVerify, async (req, res, next) => {
    const {id} = req.params;
    const {point} = req.body;
    try {
        database.query(
            "UPDATE edu_bullet_point SET point=? WHERE pointID=?",
            [point, id],
            function (error, results, fields){
                if (error) throw error;
                let updated = results.affectedRows;
                if (!updated){
                    return res.status(404).json({message: `item ${id} not found`});
                };
                return res.json(updated);
            }
        );
    } catch (err) {
        console.error(err);
        return next(err);
    };
});

export default router